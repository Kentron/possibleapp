//
//  ViewController.swift
//  Possible Mobile app
//
//  Created by Sergio Abraham on 05/12/2019.
//  Copyright © 2019 Sergio Abraham. All rights reserved.
//

import UIKit
class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var books: [Book]?
       
       func fetchBooks() {
           guard let url = URL(string: "http://de-coding-test.s3.amazonaws.com/books.json") else { return }
           URLSession.shared.dataTask(with: url) { (data, response, error) in
               
               if error != nil {
                   print(error ?? "")
                   return
               }
               
               do {
                   let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers)
                   
                   self.books = [Book]()
                   
                   for dictionary in json as! [[String: AnyObject]] {
                       
                       let book = Book()
                       book.title = dictionary["title"] as? String
                       book.imageURL = dictionary["author"] as? NSURL
                       book.thumbnailImageName = dictionary["imageURL"] as? String
                       
                       self.books?.append(book)
                   }
                   
                   DispatchQueue.main.async {
                       self.tableView?.reloadData()
                   }
                   
                   
               } catch let jsonError {
                   print(jsonError)
               }
               
               
               
           }.resume()
       }

    
    let cellReuseIdentifier = "cell"

    
    @IBOutlet var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        fetchBooks()
        
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellReuseIdentifier)
        tableView.delegate = self
        tableView.dataSource = self
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.books!.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell:UITableViewCell = (self.tableView.dequeueReusableCell(withIdentifier: cellReuseIdentifier) as UITableViewCell?)!


        cell.textLabel?.text = self.books?[indexPath.row].title
        // FIXME
       // cell.imageView?.image = self.books[indexPath.row].thunmnail

        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // FIXME: Implement this later
        print("FIXME Show detail for cell number \(indexPath.row).")
    }
}
