//
//  Book.swift
//  Possible Mobile app
//
//  Created by Sergio Abraham on 05/12/2019.
//  Copyright © 2019 Sergio Abraham. All rights reserved.
//

import UIKit

class Book: NSObject {
    
    var thumbnailImageName: String?
    var title: String?
    var imageURL: NSURL?
    
}
