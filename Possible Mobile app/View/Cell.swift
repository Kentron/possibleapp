//
//  Cell.swift
//  Possible Mobile app
//
//  Created by Sergio Abraham on 06/12/2019.
//  Copyright © 2019 Sergio Abraham. All rights reserved.
//

import UIKit
class Cell: UITableViewCell{
    
    var book: Book? { // FIXME
        didSet {
            titleLabel.text = book?.title
            
            setupThumbnailImage()
            
            
            if let title = book?.title {
               
                // FIXME if text lengh is too long
                
            }
            
        }
    }
    
    func setupThumbnailImage() {
        if let thumbnailImageUrl = book?.thumbnailImageName {
            thumbnailImageView.loadImageUsingUrlString(urlString: thumbnailImageUrl)
        }
    }
    
    let thumbnailImageView: MyImageView = {
        let imageView = MyImageView()
        imageView.image = UIImage(named: "blank")
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView
    }()
    
    
    let separatorView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha: 1)
        return view
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.text = "Unloaded"
        label.numberOfLines = 2
        return label
    }()
    
    var titleLabelHeightConstraint: NSLayoutConstraint?
    
     func setupViews() {
        addSubview(thumbnailImageView)
        addSubview(separatorView)
        addSubview(titleLabel)
        
    }
}
